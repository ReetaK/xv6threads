/* xv6 Thread change start*/

#include "types.h"
#include "stat.h"
#include "user.h"


typedef struct frisbee{
 char *token;		// Token
 int *thread;		// thread id
 int *passes;		// passes left
 lock_t *lock; 		// lock variable
}frisbee;

void *frisbeeGame(void *arg){
    frisbee *f = (frisbee*)arg;
    while(*f->passes > 0) { 			//if still passes are left
   	lock_acquire(f->lock);			// thread will acquire lock
        if(*f->passes > 0) {			// still need to check for passes left 
    		*f->thread = getpid();		// which thread
    		printf(1, "Token = %c with thread = %d in pass = %d\n",*f->token, *f->thread, *f->passes);
    		(*f->passes)--;			// decrement pass
        }
    	lock_release(f->lock);			// this thread will release the lock
    }
  return 0;
}
   

int main(int argc, char *argv[]){

  if(3 != argc){
    printf(1, "Exiting...Re-run program with arguments as Number of threads and Number of passes\n");
    exit();
  }
  
  int numThread = atoi(argv[1]);	// gets number of threads passed in command line 
  int numPass = atoi(argv[2]);		// gets number of passes passed in command line 

  lock_t lock;  			
  lock_init(&lock);			// call lock_init to initialize

  char token = 'T';			// T is token to play frisbee Game
  int thread = -1;			// initialize thread id

  frisbee f;				// frisbee to be passed in arg and assign it's fields
  f.token = &token;
  f.thread = &thread;
  f.passes = &numPass;
  f.lock = &lock;

  void *arg = (void *) &f;

  int i = 0;
  for(; i < numThread; ++i) {
   thread_create(frisbeeGame, arg);	// call thread-create routine for number of threads
  }
  for(i = 0; i < numThread; ++i) {      // wait till frisbee Game is going on
    wait();
  }
  
  printf(1, "FrisbeeGame ends... Exiting!!\n");	// Frisbee game is over and all threads exited

  exit();
}

/* xv6 Thread change start*/
