/* xv6 Thread change start */

#include "types.h"
#include "user.h"
#include "x86.h"
#include "param.h"

//create a new thread
#define PGSIZE (4096)
void thread_create(void *(*start_routine)(void*), void *arg){
  int size = PGSIZE;
  void *stack = malloc(size);		// Allocate memory for stack of new thread
  
  int cloneId = clone(stack, size);	// call clone
  if(0 == cloneId){			// if child 
   (*start_routine)(arg);		// call frisbeeGame
  exit();
  }
}

uint test_and_set_internal_lock(lock_t* lock) {
 uint internal_lock = lock->internal_lock;		
 lock->internal_lock = 1;				// set internal lock for current process
 return internal_lock;
}

void release_internal_lock(lock_t* lock) {
 lock->internal_lock = 0;				// release internal lock
}

int fetch_and_inc(lock_t* lock) {
 while (test_and_set_internal_lock(lock) == 1);		// Can't access fetch and increment as some other process is accessing rightnow
 int temp = lock->queuelast;				// gets its index in queue
 lock->queuelast = (temp + 1)%NPROC;			// queuelast increments 
 release_internal_lock(lock);				// release interna lock for fetch and increment
 return temp;						// return index for that process
}


void lock_init(lock_t *lock){
  //lock->locked = 0;
  lock->flags[0] = 1;					// sets first index in queue as locked
  int ind = 1;
  for(; ind < NPROC; ++ind){				// sets rest other index as unlocked
    lock->flags[ind] = 0;
  }
  lock->queuelast = 0; 					// initialize queuelast with first index in queue
  lock->internal_lock = 0;				// initialize internal lock as unlocked
  return;
}

void lock_acquire(lock_t *lock){
  //while(xchg(&lock->locked , 1) != 0);    //if lock is held
  int myplace = fetch_and_inc(lock);        // Each process will gets its index in queue to spin
  setLock(myplace);			    // Store in proc struct
  while(lock->flags[myplace % NPROC] == 0);  // Spin while the value at its index is 0
  return;				     // Value at its index is now 1 thus got lock
}

void lock_release(lock_t *lock){
 // xchg(&lock->locked, 0);
  int procPlace = getLock();			// gets its index from proc struct
  if (procPlace < 0 && procPlace >= NPROC)	// If its not valid index 
  {
    printf(1, "Playing games by calling release before lock");
    exit();
  }
  
  if (lock->flags[procPlace] == 1) {		
   lock->flags[procPlace] = 0;			// Assign its index as 0 means it now hasn't acquired lock
   lock->flags[(procPlace+1) % NPROC] = 1;	// Locks get assigned to the next index
  }
  
  return;
}

/* xv6 Thread change end */
